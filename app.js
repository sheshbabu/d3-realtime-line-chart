var express = require('express'),
    app = express(),
    http = require('http').Server(app),
    io = require('socket.io')(http),
    RandomDataGenerator = require('./random-data-generator').RandomDataGenerator;

var dataSource = new RandomDataGenerator();

app.use(express.static(__dirname + '/static'));

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/static/index.htm');
});

io.on('connection', function(socket) {
    dataSource.on('data', function(data) {
        io.emit('data', data);
    });
});

http.listen(3000);