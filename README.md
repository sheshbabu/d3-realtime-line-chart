d3-realtime-line-chart
======================

Realtime line chart using d3.js

![Screenshot]
(./screenshot.png)

Installation
------------
Clone it into your machine

    git clone https://github.com/sheshbabu/d3-realtime-line-chart.git

cd into that directory and install dependencies

    sudo npm install

Start the server

    node app.js

Navigate to the following url in your browser

    localhost:3000